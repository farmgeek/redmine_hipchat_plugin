require 'redmine'

Rails.configuration.to_prepare do
  require_dependency 'issue_patch'
  require_dependency 'journal_patch'

  Issue.send(:include, IssuePatch)
  Journal.send(:include, JournalPatch)
end

Redmine::Plugin.register :redmine_hipchat_per_project do
  name        'Custom Hipchat plugin'
  author      'John Hamelink <john@farmer.io>'
  description 'Hipchat notifications for projects'
  version     '0.0.1'
  url         'https://bitbucket.com/farmgeek/redmine_hipchat_plugin'

  project_module :hipchat do
  end

  settings :default => {}
end
